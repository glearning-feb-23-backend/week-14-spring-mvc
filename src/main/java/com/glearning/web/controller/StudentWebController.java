package com.glearning.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/student")
public class StudentWebController {
	
	@RequestMapping("/")
	public String showPage(Model model) {
		model.addAttribute("user", "Pradeep");
		return "main-menu";
	}

}
