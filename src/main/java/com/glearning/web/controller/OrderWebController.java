package com.glearning.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/orders")
public class OrderWebController {
	
	@RequestMapping("/")
	public String showPage() {
		return "main-menu";
	}


}
